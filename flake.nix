{
	description = "my installed fonts";

	inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

	outputs = { self, nixpkgs, ... }: {
		packages.x86_64-linux.default = (import nixpkgs { system = "x86_64-linux"; }).stdenv.mkDerivation {
			name = "myfonts";
			src = ./.;
			installPhase = ''mkdir -p $out/share; cp -r fonts $out/share'';
		};
	};
}